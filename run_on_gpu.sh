#!/bin/bash
#SBATCH --partition=gwendolen         # Or 'gpu-short' for higher priority but 2-hour limit
#SBATCH --account=gwendolen         # Or 'gpu-short' for higher priority but 2-hour limit
#SBATCH --cluster=gmerlin6      # Required for GPU
#SBATCH --gpus=1                # Multi GPU not supported
#SBATCH --cpus-per-gpu=1        # Request CPU resources
#SBATCH --mem-per-cpu=16G
#SBATCH --time=1:00:00       # Define max time job will run
#SBATCH --output=RLN.out   # Define your output file
#SBATCH --error=RLN.err    # Define your error file

module purge
module load cuda/10.0.130

srun apptainer run --nv --bind /data/user/tekin_g/rln0_data/data:/data /data/user/tekin_g/rln0.sif --mode TR #1. replace /data/user/tekin_g/rln0_data with your data path. 2. arguments after .sif are passed to the python file.